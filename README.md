# dingtalk-api

## 介绍

钉钉开放平台SDK代码

来源地址：https://ding-doc.dingtalk.com/doc#/faquestions/vzbp02

## 使用方法

### 安装

下载本项目,执行maven安装命令：`mvn install`

在pom文件中引入starter
```xml
<dependency>
    <artifactId>dingtalk-spring-boot-starter</artifactId>
    <groupId>com.taobao</groupId>
    <version>1.0</version>
</dependency>
```

### 项目引用

在Spring Boot项目中加入注解`@EnableDingTalk`
```java
@EnableDingTalk
@SpringBootApplication
public class SpringBootDemoApplication {
    


}
```

在`application.yml`配置文件中配置钉钉相关属性
```yaml
dingtalk:
    appid: xxx
    appsecret: xxxxxxxxxxxx
    agentid: 1243456
```

业务方法中使用消息类通知如下：

```java
@Service
public class MessageService{

    @Autowired
    DingTalkMessagePushService dingTalkMessagePushService;
    
    public void sendText(){
        DingMessageText dt=new DingMessageText("我是文本消息啦---");
        //全员推送工作消息
        dingTalkMessagePushService.workMessage(null,null,true,dt);
    }   

}
```