/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.taobao.dingtalk.spring.configuration;

import com.taobao.dingtalk.client.DingTalkClientToken;
import com.taobao.dingtalk.service.message.DingTalkMessagePushService;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/***
 *
 * @since:dingtalk-api 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2019/11/28 9:12
 */
@Configuration
@EnableConfigurationProperties(DingTalkProperties.class)
public class DingTalkConfiguration {

    /**
     * 注入Token-Model
     * @param dingTalkProperties
     * @return
     */
    @Bean
    public DingTalkClientToken dingTalkClientToken(DingTalkProperties dingTalkProperties){
        return new DingTalkClientToken(dingTalkProperties.getAppid(),dingTalkProperties.getAppsecret(),dingTalkProperties.getAgentid());
    }


    /**
     * 消息推送实体
     * @param dingTalkClientToken
     * @return
     */
    @Bean
    public DingTalkMessagePushService dingTalkMessagePushService(DingTalkClientToken dingTalkClientToken){
        return new DingTalkMessagePushService(dingTalkClientToken);
    }





}
