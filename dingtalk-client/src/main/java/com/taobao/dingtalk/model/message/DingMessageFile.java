/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.taobao.dingtalk.model.message;

import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.Map;

/***
 *
 * @since:dingtalk-api 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2019/11/28 10:43
 */
public class DingMessageFile extends DingMessage {

    private final String mediaId;

    public DingMessageFile(String mediaId) {
        super(MESSAGE_FILE);
        this.mediaId = mediaId;
    }

    @Override
    public String toString() {
        //得到文本消息类JSON字符串
        Map<String,Object> map=getDefaultMap();
        Map<String,Object> mediaId=new HashMap<>();
        mediaId.put("media_id",mediaId);
        map.put("file",mediaId);
        return JSON.toJSONString(map);
    }
}
