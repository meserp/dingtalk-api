/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.taobao.dingtalk.model.message;

import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.Map;

/***
 *
 * @since:dingtalk-api 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2019/11/28 10:52
 */
public class DingMessageMarkdown extends DingMessage {

    private final String title;
    private final String content;

    public DingMessageMarkdown(String title, String content) {
        super(MESSAGE_MARKDOWN);
        this.title = title;
        this.content = content;
    }

    @Override
    public String toString() {
        //得到文本消息类JSON字符串
        Map<String,Object> map=getDefaultMap();
        Map<String,Object> markdown=new HashMap<>();
        markdown.put("title",title);
        markdown.put("text",content);

        map.put("markdown",markdown);
        return JSON.toJSONString(map);
    }
}
