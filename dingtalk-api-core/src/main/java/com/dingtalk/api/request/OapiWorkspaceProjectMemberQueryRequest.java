package com.dingtalk.api.request;

import java.util.List;
import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.internal.util.RequestCheckUtils;
import com.taobao.api.TaobaoObject;
import java.util.Map;
import java.util.List;

import com.taobao.api.ApiRuleException;
import com.taobao.api.BaseTaobaoRequest;
import com.dingtalk.api.DingTalkConstants;
import com.taobao.api.Constants;
import com.taobao.api.internal.util.TaobaoHashMap;
import com.taobao.api.internal.util.TaobaoUtils;
import com.taobao.api.internal.util.json.JSONWriter;
import com.dingtalk.api.response.OapiWorkspaceProjectMemberQueryResponse;

/**
 * TOP DingTalk-API: dingtalk.oapi.workspace.project.member.query request
 * 
 * @author top auto create
 * @since 1.0, 2019.09.16
 */
public class OapiWorkspaceProjectMemberQueryRequest extends BaseTaobaoRequest<OapiWorkspaceProjectMemberQueryResponse> {
	
	

	/** 
	* 查询项目成员参数 最多20个
	 */
	private String members;

	public void setMembers(String members) {
		this.members = members;
	}

	public void setMembers(List<OpenMemberQueryDto> members) {
		this.members = new JSONWriter(false,false,true).write(members);
	}

	public String getMembers() {
		return this.members;
	}

	public String getApiMethodName() {
		return "dingtalk.oapi.workspace.project.member.query";
	}

	private String topResponseType = Constants.RESPONSE_TYPE_DINGTALK_OAPI;

     public String getTopResponseType() {
        return this.topResponseType;
     }

     public void setTopResponseType(String topResponseType) {
        this.topResponseType = topResponseType;
     }

     public String getTopApiCallType() {
        return DingTalkConstants.CALL_TYPE_OAPI;
     }

     private String topHttpMethod = DingTalkConstants.HTTP_METHOD_POST;

     public String getTopHttpMethod() {
     	return this.topHttpMethod;
     }

     public void setTopHttpMethod(String topHttpMethod) {
        this.topHttpMethod = topHttpMethod;
     }

     public void setHttpMethod(String httpMethod) {
         this.setTopHttpMethod(httpMethod);
     }

	public Map<String, String> getTextParams() {		
		TaobaoHashMap txtParams = new TaobaoHashMap();
		txtParams.put("members", this.members);
		if(this.udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public Class<OapiWorkspaceProjectMemberQueryResponse> getResponseClass() {
		return OapiWorkspaceProjectMemberQueryResponse.class;
	}

	public void check() throws ApiRuleException {
		RequestCheckUtils.checkObjectMaxListSize(members, 20, "members");
	}
	
	/**
	 * 查询项目成员参数 最多20个
	 *
	 * @author top auto create
	 * @since 1.0, null
	 */
	public static class OpenMemberQueryDto extends TaobaoObject {
		private static final long serialVersionUID = 5797357846986294717L;
		/**
		 * 成员id
		 */
		@ApiField("userid")
		private String userid;
	
		public String getUserid() {
			return this.userid;
		}
		public void setUserid(String userid) {
			this.userid = userid;
		}
	}
	

}